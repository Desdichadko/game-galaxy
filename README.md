# GameGalaxy pet project

## Main pages:

---

* [Main shop page](https://game-galaxy-shop.herokuapp.com/games)

* [Authorization page](https://game-galaxy-shop.herokuapp.com/sign-in)

* [My account page](https://game-galaxy-shop.herokuapp.com/my-account)

* [Main ADMIN page](https://game-galaxy-shop.herokuapp.com/admin/games)
For editing, adding games.
NOTE: Available only after authorization with admin access.

## Additional information
Test ADMIN account:
e-mail: admin@admin
password: admin